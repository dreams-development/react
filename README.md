# React
## Docs
- [React Docs](https://reactjs.org/docs/hello-world.html)
- [Posts](https://reactjs.org/blog/all.html)

## Articles
### Common
- [The one thing that no one properly explains about React — Why Virtual DOM](https://hashnode.com/post/the-one-thing-that-no-one-properly-explains-about-react-why-virtual-dom-cisczhfj41bmssp53mvfwmgrq)
- [Как работает Virtual DOM ?](https://medium.com/@abraztsov/how-virtual-dom-work-567128ed77e9)
- [Understanding React — Component life-cycle](https://medium.com/@baphemot/understanding-reactjs-component-life-cycle-823a640b3e8d), [ React 16.3](https://medium.com/@baphemot/understanding-react-react-16-3-component-life-cycle-23129bc7a705)
- [Writing Scalable React Apps with the Component Folder Pattern](https://medium.com/styled-components/component-folder-pattern-ee42df37ec68)
- [Structuring projects and naming components in React](https://hackernoon.com/structuring-projects-and-naming-components-in-react-1261b6e18d76)
- [Best Practices for Writing React Components](https://engineering.musefind.com/our-best-practices-for-writing-react-components-dec3eb5c3fc8)
- [Optimizing React Rendering](https://flexport.engineering/optimizing-react-rendering-part-1-9634469dca02)
- [Higher-Order Components: The Ultimate Guide](https://medium.freecodecamp.org/higher-order-components-the-ultimate-guide-b453a68bb851)
- [Using render props](https://hackernoon.com/do-more-with-less-using-render-props-de5bcdfbe74c)
- [Understanding The React Source Code](https://hackernoon.com/understanding-the-react-source-code-initial-rendering-simple-component-i-80263fe46cf1)
- [Refactoring In React](https://codeburst.io/refactoring-in-react-9f6859c41d9)
- [React patterns](https://github.com/krasimir/react-in-patterns)
- [Clean Code vs. Dirty Code: React Best Practices](http://americanexpress.io/clean-code-dirty-code/)
- [The most important lessons I’ve learned after a year of working with React](https://medium.freecodecamp.org/mindset-lessons-from-a-year-with-react-1de862421981)
- [Techniques for decomposing React components](https://medium.com/dailyjs/techniques-for-decomposing-react-components-e8a1081ef5da)

### Redux
- [Understanding Redux Middleware](https://medium.com/@meagle/understanding-87566abcfb7a)
- [Five Tips for Working with Redux in Large Applications](https://techblog.appnexus.com/five-tips-for-working-with-redux-in-large-applications-89452af4fdcb)
- [Async operations using redux-saga](https://wecodetheweb.com/2016/10/01/handling-async-in-redux-with-sagas/)
- [This collection of common Redux-saga patterns will make your life easier.](https://medium.freecodecamp.org/redux-saga-common-patterns-48437892e11c)

### Testing
- [Some Thoughts On Testing React/Redux Applications](https://medium.com/javascript-inside/some-thoughts-on-testing-react-redux-applications-8571fbc1b78f)
- [Testing React components with Jest and Enzyme](https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f)
- [Testing React Components Best Practices](https://medium.com/selleo/testing-react-components-best-practices-2f77ac302d12)
- [React + TDD = 💖](https://medium.com/@admm/test-driven-development-in-react-is-easy-178c9c520f2f)

### Tools & Vidos & Helpers
- [Better React Forms with Formik](https://youtu.be/yNiJkjEwmpw)
- [Painless React Forms with Formik](https://hackernoon.com/painless-react-forms-with-formik-e61b70473c60)
- [Animation of React Components](https://www.youtube.com/watch?v=npvQX53YuCs)
- [Modals in React and Redux Apps](https://www.youtube.com/watch?v=WGjv-p9jYf0)
- [React Components & Libraries](https://github.com/brillout/awesome-react-components)
- [Using a React 16 Portal to do something cool](https://hackernoon.com/using-a-react-16-portal-to-do-something-cool-2a2d627b0202)
- [Combining React with Socket.io for real-time goodness](https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34)

